fastapi==0.54.1
pydantic==1.5.1
uvicorn==0.11.3
passlib==1.7.2
gunicorn==20.0.4
PyJWT==1.7.1
py==1.8.1
jwt
